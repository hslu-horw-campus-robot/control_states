#!/usr/bin/env python

import rospy
import smach
import smach_ros
import time

from std_msgs.msg import Bool
from std_msgs.msg import String
from std_msgs.msg import Int16
from std_msgs.msg import Empty
from geometry_msgs.msg import PoseStamped


sis : smach_ros.IntrospectionServer
int_selection = 'none'
# list of all supported locations of guiding package
locations_list=['canteen', 'library', 'wing2', 'wing5', 'test', 'home']
got_close = False


def indicate_activity():
    # bark and turn head lights to "mixed colors"
    rospy.loginfo('indicating activity')
    # color value 3 means mixed, see https://gitlab.switch.ch/hslu_campus_robot/headlight_sdk/-/tree/master?ref_type=heads#ros-node-interface
    rospy.set_param('face_light/face_light_color', 3)
    lights_pub = rospy.Publisher('/go_lights', Empty, queue_size=10)
    lights_pub.publish()
    audio_pub = rospy.Publisher('/go_bark', Int16, queue_size=10)
    audio_pub.publish(3)


def indicate_end_of_activity():
    # bark and turn head lights off
    rospy.loginfo('indicating end of activity')
    # color value 0 means black/off, see https://gitlab.switch.ch/hslu_campus_robot/headlight_sdk/-/tree/master?ref_type=heads#ros-node-interface
    rospy.set_param('face_light/face_light_color', 0)
    lights_pub = rospy.Publisher('/go_lights', Empty, queue_size=10)
    lights_pub.publish()
    audio_pub = rospy.Publisher('/go_bark', Int16, queue_size=10)
    audio_pub.publish(1)


# define states
class Startup(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        global int_selection
        rospy.loginfo('Executing state Startup')
        int_selection = 'none'
        # turn on autonomous sequences and/or autonomous sounds
        # activate web ui
        ui_pub = rospy.Publisher('/ui_active', Bool, queue_size=10)
        ui_pub.publish(True)
        return 'done'


# interaction selection
class Intselector(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['int1', 'int2', 'int3', 'int4'])

    def execute(self, userdata):
        global int_selection
        rospy.loginfo('Executing state Intselector')
        # deactivate ui for further inputs
        ui_pub = rospy.Publisher('/ui_active', Bool, queue_size=10)
        ui_pub.publish(False)
        indicate_activity()
        if int_selection == 'bottle':
            return 'int2'
        elif int_selection == 'demo':
            return 'int3'
        elif int_selection in locations_list:
            return 'int4'
        else:
            return 'int1'


#this callback handles ui requests (std_msgs/String)
def idle_monitor_cb(user_data, msg):
    global int_selection
    rospy.loginfo('Executing Idle callback')
    int_selection = msg.data
    print(int_selection)
    return False


# interaction 1
class Int1(smach.State):
    # Default interaction: do nothing
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Int1')

        return 'done'


class Int2(smach.State):
    # Interaction: get a bottle of water from canteen
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        global got_close
        rospy.loginfo('Executing state Int2')
        got_close = False
        # got close can be triggered with rostopic pub /goto_goal .. "halt" -1
        while not got_close:
            time.sleep(0.5)
        return 'done'


class Int3(smach.State):
    # Interaction: demo (e.g. do a trick)
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Int3')
        song_pub = rospy.Publisher('/go_bark', Int16, queue_size=10)
        song_pub.publish(15)
        time.sleep(3)
        song_pub.publish(0)
        return 'done'


class Int4(smach.State):
    # Selected interaction: guiding to a specific location
    # Node goal_request_translator takes care of the guiding itself
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        global got_close
        rospy.loginfo('Executing state Int4')
        pos_pub = rospy.Publisher('/goto_goal', String, queue_size=10)
        pos_pub.publish(int_selection)
        got_close = False
        while not got_close:
            time.sleep(0.5)
        return 'done'


class Homing(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['heading_home'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Homing')
        time.sleep(3)
        # activate web ui
        ui_pub = rospy.Publisher('/ui_active', Bool, queue_size=10)
        ui_pub.publish(True)
        # publish new goal (go1 home)
        pos_pub = rospy.Publisher('/goto_goal', String, queue_size=10)
        # todo: add a sensible 'home' to go1/guiding !
        #       with publishing 'halt' go1 will stay at the current location in state "homing"
        pos_pub.publish('halt')
        indicate_end_of_activity()
        return 'heading_home'


def reached_cb(data):
    global got_close
    got_close = True


def stop_sis_server():
    global sis
    sis.stop()


def main():
    global sis
    rospy.init_node('sm_main')
    rospy.on_shutdown(stop_sis_server)

    rospy.Subscriber("guiding/reached", PoseStamped, reached_cb)

    # Create the top level SMACH state machine of (possibly) nested state machines for go1 control
    go1_sm_top = smach.StateMachine(outcomes=['shutting_down']) # -> dummy outcome, required because of sm cycles.

    # Open the container
    with go1_sm_top:
        smach.StateMachine.add('STARTUP', Startup(),
                               transitions={'done':'IDLE'})

        # it seems that the callback allways has to return false. otherwise the transition did not happen.
        smach.StateMachine.add('IDLE', smach_ros.MonitorState('/ui_request', String, idle_monitor_cb),
                               transitions={'valid':'INTSELECTOR', 'invalid':'INTSELECTOR', 'preempted':'shutting_down'})

        smach.StateMachine.add('INTSELECTOR', Intselector(),
                               transitions={'int1': 'INT1', 'int2': 'INT2', 'int3': 'INT3', 'int4': 'INT4'})

        # todo: if requried sub states can be added to Int.. states, see:
        #http://wiki.ros.org/smach/Tutorials/Create%20a%20hierarchical%20state%20machine
        smach.StateMachine.add('INT1', Int1(),
                               transitions={'done':'HOMING'})

        smach.StateMachine.add('INT2', Int2(),
                               transitions={'done':'HOMING'})

        smach.StateMachine.add('INT3', Int3(),
                               transitions={'done':'HOMING'})

        smach.StateMachine.add('INT4', Int4(),
                               transitions={'done':'HOMING'})

        smach.StateMachine.add('HOMING', Homing(),
                               transitions={'heading_home':'IDLE'})

    sis = smach_ros.IntrospectionServer('smach_server', go1_sm_top, '/SM_ROOT')
    sis.start()

    # Execute SMACH plan
    outcome = go1_sm_top.execute()
    sis.stop()

if __name__ == '__main__':
    main()
