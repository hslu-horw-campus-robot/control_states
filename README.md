# control_states (own go1 specific) ros package

State machine for go1 control:
![smach_viewer screenshot](smach_viewer.png)

## Status
State machine has been tested on Go1 successfully. 

## Testing / controlling the state machine
The state machine can be tested by running it standalone:
```
roscore
rosrun control_states state_main.py
```
The state machine can be controlled such (final ```-1``` means: publish for 3s only):

Transition from state IDLE via state INTSELECTOR to state INT1 (interaction1, guiding, "canteen" is only an example, other strings/locations will be supported):
```
rostopic pub /ui_request std_msgs/String "canteen" -1
```
Transition from state IDLE via state INTSELECTOR to state INT2 (interaction2, get water bottle):
```
rostopic pub /ui_request std_msgs/String "bottle" -1
```
Transition from state IDLE via state INTSELECTOR to state INT3 (interaction3, demo):
```
rostopic pub /ui_request std_msgs/String "demo" -1
```

### Emergency stop
If Go1 gets lost (e.g. in recovery turns or abandons goal) or if it does not find a goal and you want to stop it publish this command on terminal:
```
rostopic pub /goto_goal std_msgs/String "halt" -1
```
This command should be processed by guiding node to set a new goal at the current location (and stop there).

### Demo use cases
Somehow the first time method indicate_activity is called it is not executed (needs a fix). As a workaround for the demo / testing run to **init the state machine** "empty" once with this command:
```
rostopic pub /ui_request std_msgs/String "anything" -1
```
**Demo / Sing a song** can be executed with the UI button.
```
rostopic pub /ui_request std_msgs/String "demo" -1
```
The **bottle** use case can be started with the UI botton. It turns on the head lights after barking. End the bottle use case by publishing this command:
```
rostopic pub /goto_goal std_msgs/String "halt" -1
```
Note that this time it must be published to topic `/goto_goal`

## Debugging the state machine
### With smach rostopics 
You can view the **current state** of the state machine with this command:
```
rostopic echo /smach_server/smach/container_status 
```
"active_states" then indicates the currently active state:
```
---
header: 
  seq: 784
  stamp: 
    secs: 1700837331
    nsecs: 465276002
  frame_id: ''
path: "/SM_ROOT"
initial_states: [STARTUP]
active_states: [IDLE]
local_data: !!binary |
  gAJ9cQAu
info: "HEARTBEAT"
---
```

### With smach_viewer
- install smach_viewer with
```
sudo apt-get install ros-noetic-smach-viewer
```
- make sure that the introspection server is running (see file substate_mowing.py: smach_ros.IntrospectionServer)
- if you have conda (virtual environment) installed deactivate all environments, also (base)
- launch smach_viewer with
```
rosrun smach_viewer smach_viewer.py
```
- set Path to /SM_ROOT to get the graph view
- the active state is green
- you should now see the state machine as at the top of this readme


